move = -key_left + key_right;
hsp = move*movespeed;

if(vsp<10)vsp+=grav;

if(place_meeting(x,y+1,obj_wall) || place_meeting(x,y+1,obj_ladder)) {
    jumping = false;   
    vsp = key_jump * -jumpspeed;
    if (key_jump == true) {
        key_jump = false;
        jumping = true;
    }
    
}


//horizontal collision

var blck = instance_place(x+hsp,y,obj_wall);
if(blck != noone){
    x = blck.x - sign(hsp)*((blck.col_width + col_width)/2 + 2);
}else{
    x += hsp;
}

/*
if (place_meeting(x+hsp,y,obj_wall))
{
    while(!place_meeting(x+sign(hsp),y,obj_wall))
    {
        x += sign(hsp);
    }
    hsp = 0;
}
x += hsp;
*/

//vertical collision

/*
var blcky = instance_place(x,y+vsp+grav,obj_wall);
if(blcky != noone){
    y = blcky.y - sign(vsp)*((blcky.col_width + col_width)/2 + 2);
    vsp = 0;
}else{
    y += vsp;
}
*/

if (place_meeting(x,y+vsp,obj_wall)){
    while(!place_meeting(x,y+sign(vsp),obj_wall))
    {
        y += sign(vsp);
    }
    vsp = 0;
}
y += vsp;



// Ladder
if (place_meeting(x,y,obj_ladder) && key_up) {
    climbing = true;
    y -= 2;
} else if (!place_meeting(x,y+2,obj_wall) && place_meeting(x,y + 1,obj_ladder) && key_down){
    y += 2;
    climbing = true;
}

if (!place_meeting(x,y,obj_ladder)) {
    climbing = false;
}

if (place_meeting(x,y+1,obj_ladder) && !climbing && !jumping) {
    vsp =  0;
}

if (place_meeting(x,y+2,obj_wall) && key_down) {
    climbing = false;
}


if (climbing && key_jump){
    jumping = true;
    climbing = false;
}

if (climbing){
    jumping = false;
    grav = 0;
    vsp = 0;
} else if (!climbing){
    grav = 0.2;
}


// Fear of heights
if (place_meeting(x,y+8,obj_wall)) {
    if (fearofheights == true && !position_meeting(x+((sprite_width/2)*dir), y + (sprite_height/2)+8, obj_wall) && position_meeting(x+(100*dir), y + (sprite_height/2)+8, obj_wall)) {
        key_jump = true;
    }
    else if (fearofheights == true && !position_meeting(x+((sprite_width/2)*dir), y + (sprite_height/2)+8, obj_wall)) {
        dir *= -1;
    }
}


